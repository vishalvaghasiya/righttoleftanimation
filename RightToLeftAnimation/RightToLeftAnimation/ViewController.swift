//
//  ViewController.swift
//  RightToLeftAnimation
//
//  Created by MAC on 01/07/19.
//  Copyright © 2019 Vishal. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CAAnimationDelegate {

    @IBOutlet weak var swipeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(rightGesture(gesture:)))
        swipeRight.direction = .right
        swipeView.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(rightGesture(gesture:)))
        swipeLeft.direction = .left
        swipeView.addGestureRecognizer(swipeLeft)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func rightGesture(gesture: UIGestureRecognizer){
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                viewSlideInFromLeftToRight(views: swipeView)
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                viewSlideInFromRightToLeft(views: swipeView)
            default:
                break
            }
        }
    }

    func viewSlideInFromRightToLeft(views:UIView) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut) //kCAMediaTimingFunctionEaseInEaseOut
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.delegate = self
        views.layer.add(transition, forKey: nil)
    }
    
    func viewSlideInFromLeftToRight(views:UIView) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut) //kCAMediaTimingFunctionEaseInEaseOut
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.delegate = self
        views.layer.add(transition, forKey: nil)
    }
    
}

